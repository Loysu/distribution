import pytest
from fastapi import status
from fastapi.testclient import TestClient

from tests.utils import (
    CLIENT_MOBILE_CODE_OPERATOR,
    CLIENT_PHONE_NUMBER,
    DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE,
    DISTRIBUTION_MESSAGE_TEXT,
    DISTRIBUTION_START,
    DISTRIBUTION_STOP,
    MESSAGE_DELIVERED,
    MESSAGE_STATE,
    MESSAGE_STATE_UPDATED,
    TAG_NAME,
    create_client,
    create_distribution,
    create_message,
    create_tag,
    delete_message,
    get_message,
    update_message,
)

pytestmark = pytest.mark.usefixtures("use_postgres")

CLIENT_DATA = dict(
    phone_number=CLIENT_PHONE_NUMBER,
    mobile_operator_code=CLIENT_MOBILE_CODE_OPERATOR,
    tags=[TAG_NAME],
)
DISTRIBUTION_DATA = dict(
    start=DISTRIBUTION_START,
    stop=DISTRIBUTION_STOP,
    message_text=DISTRIBUTION_MESSAGE_TEXT,
    client_mobile_operator_code=DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE,
    tags=[TAG_NAME],
)
MESSAGE_DATA = dict(delivered=MESSAGE_DELIVERED, state=MESSAGE_STATE)
MESSAGE_DATA_UPDATED = MESSAGE_DATA.copy()
MESSAGE_DATA_UPDATED["state"] = MESSAGE_STATE_UPDATED


class TestCreateMessage:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        response = create_message(api_client=api_client, message_data=message_data)

        assert response.status_code == status.HTTP_201_CREATED
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("delivered") == MESSAGE_DELIVERED
        assert data.get("state") == MESSAGE_STATE.title()
        assert data.get("client_id") == client.json().get("id")
        assert data.get("distribution_id") == distribution.json().get("id")

    def test_invalid_state(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")
        message_data["state"] = "some state"

        response = create_message(api_client=api_client, message_data=message_data)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestGetMessage:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        message = create_message(api_client=api_client, message_data=message_data)
        message_id = message.json().get("id")
        response = get_message(api_client=api_client, message_id=message_id)

        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("delivered") == MESSAGE_DELIVERED
        assert data.get("state") == MESSAGE_STATE.title()
        assert data.get("client_id") == client.json().get("id")
        assert data.get("distribution_id") == distribution.json().get("id")

    def test_invalid_id(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        create_message(api_client=api_client, message_data=message_data)

        response = get_message(api_client=api_client, message_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestUpdateMessage:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        message = create_message(api_client=api_client, message_data=message_data)
        message_id = message.json().get("id")
        updated_message_data = MESSAGE_DATA_UPDATED.copy()
        updated_message_data["id"] = message_id

        response = update_message(
            api_client=api_client, message_data=updated_message_data
        )

        assert response.status_code == status.HTTP_202_ACCEPTED
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert data.get("delivered") == MESSAGE_DELIVERED
        assert data.get("state") == MESSAGE_STATE_UPDATED.title()
        assert data.get("client_id") == client.json().get("id")
        assert data.get("distribution_id") == distribution.json().get("id")

    def test_invalid_state(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        message = create_message(api_client=api_client, message_data=message_data)

        updated_message_data = MESSAGE_DATA_UPDATED.copy()
        updated_message_data["id"] = message.json().get("id")
        updated_message_data["state"] = "some state"

        response = update_message(
            api_client=api_client, message_data=updated_message_data
        )

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestDeleteMessage:
    def test_ok(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        message = create_message(api_client=api_client, message_data=message_data)
        message_id = message.json().get("id")

        response = delete_message(api_client=api_client, message_id=message_id)

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert response.json() is None

        message = get_message(api_client=api_client, message_id=message_id)

        assert message.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert message.json() is not None

    def test_invalid_id(self, api_client: TestClient) -> None:
        create_tag(api_client=api_client, tag_name=TAG_NAME)

        distribution = create_distribution(
            api_client=api_client, distribution_data=DISTRIBUTION_DATA
        )
        client = create_client(api_client=api_client, client_data=CLIENT_DATA)

        message_data = MESSAGE_DATA.copy()
        message_data["client_id"] = client.json().get("id")
        message_data["distribution_id"] = distribution.json().get("id")

        message = create_message(api_client=api_client, message_data=message_data)
        message_id = message.json().get("id")

        response = delete_message(api_client=api_client, message_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None

        message = get_message(api_client=api_client, message_id=message_id)

        assert message.status_code == status.HTTP_200_OK
        assert message.json() is not None
