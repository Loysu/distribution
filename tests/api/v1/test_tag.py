import pytest
from fastapi import status
from fastapi.testclient import TestClient

from tests.utils import TAG_NAME, create_tag, delete_tag, get_tag

pytestmark = pytest.mark.usefixtures("use_postgres")


class TestCreateTag:
    def test_ok(self, api_client: TestClient) -> None:
        response = create_tag(api_client=api_client, tag_name=TAG_NAME)

        assert response.status_code == status.HTTP_201_CREATED
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert isinstance(data.get("name"), str)
        assert data.get("name") == TAG_NAME

    def test_invalid_data(self, api_client: TestClient) -> None:
        response = create_tag(api_client=api_client, tag_name=[123, "123"])

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestGetTag:
    def test_ok(self, api_client: TestClient) -> None:
        tag = create_tag(api_client=api_client, tag_name=TAG_NAME)
        tag_data = tag.json()

        response = get_tag(api_client=api_client, tag_id=tag_data.get("id"))

        assert response.status_code == status.HTTP_200_OK
        data = response.json()
        assert isinstance(data.get("id"), int)
        assert isinstance(data.get("name"), str)
        assert data.get("name") == TAG_NAME

    def test_invalid_id(self, api_client: TestClient) -> None:
        tag = create_tag(api_client=api_client, tag_name=TAG_NAME)
        tag.json()

        response = get_tag(api_client=api_client, tag_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None


class TestDeleteTag:
    def test_ok(self, api_client: TestClient) -> None:
        tag = create_tag(api_client=api_client, tag_name=TAG_NAME)
        tag_data = tag.json()

        response = delete_tag(api_client=api_client, tag_id=tag_data.get("id"))

        assert response.status_code == status.HTTP_204_NO_CONTENT
        assert response.json() is None

        tag = get_tag(api_client=api_client, tag_id=tag_data.get("id"))

        assert tag.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert tag.json() is not None

    def test_invalid_id(self, api_client: TestClient) -> None:
        tag = create_tag(api_client=api_client, tag_name=TAG_NAME)
        tag_data = tag.json()

        response = delete_tag(api_client=api_client, tag_id=-1)

        assert response.status_code == status.HTTP_422_UNPROCESSABLE_ENTITY
        assert response.json() is not None

        tag = get_tag(api_client=api_client, tag_id=tag_data.get("id"))

        assert tag.status_code == status.HTTP_200_OK
        assert tag.json() is not None
