from fastapi.testclient import TestClient

CLIENT_PHONE_NUMBER = "+79053268476"
CLIENT_MOBILE_CODE_OPERATOR = 10

CLIENT_PHONE_NUMBER_UPDATED = "89053268476"
CLIENT_MOBILE_CODE_OPERATOR_UPDATED = 100


def create_client(api_client: TestClient, client_data):
    return api_client.post("v1/client/", json=client_data)


def get_client(api_client: TestClient, client_id):
    return api_client.get(f"v1/client/{client_id}")


def update_client(api_client: TestClient, client_data):
    return api_client.put("v1/client/", json=client_data)


def delete_client(api_client: TestClient, client_id):
    return api_client.delete(f"v1/client/{client_id}")
