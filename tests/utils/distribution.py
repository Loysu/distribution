from datetime import datetime, timedelta

from fastapi.encoders import jsonable_encoder
from fastapi.testclient import TestClient

DISTRIBUTION_START = jsonable_encoder(datetime.now())
DISTRIBUTION_STOP = jsonable_encoder(datetime.now() + timedelta(seconds=3))
DISTRIBUTION_MESSAGE_TEXT = "text"
DISTRIBUTION_CLIENT_MOBILE_OPERATOR_CODE = 10

DISTRIBUTION_MESSAGE_TEXT_UPDATED = "updated text"


def create_distribution(api_client: TestClient, distribution_data):
    return api_client.post("v1/distribution/", json=distribution_data)


def get_distribution(api_client: TestClient, distribution_id):
    return api_client.get(f"v1/distribution/{distribution_id}")


def update_distribution(api_client: TestClient, distribution_data):
    return api_client.put("v1/distribution/", json=distribution_data)


def delete_distribution(api_client: TestClient, distribution_id):
    return api_client.delete(f"v1/distribution/{distribution_id}")


def send_distributions(api_client: TestClient, auth_token, start):
    data = dict(auth_token=auth_token, start=start)
    return api_client.post("v1/distribution/send", json=data)
