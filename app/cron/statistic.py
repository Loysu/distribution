import json
import os
import smtplib
import ssl

import requests

from app.cron.cron import API_HOST

port = 465
SMTP_SERVER = os.getenv("SMTP_SERVER")
SENDER_EMAIL = os.getenv("SENDER_EMAIL")
RECEIVER_EMAIL = os.getenv("RECEIVER_EMAIL")
EMAIL_PASSWORD = os.getenv("EMAIL_PASSWORD")


def send_email_stats_to_email():
    response = requests.get(
        f"{API_HOST}/v1/statistic/distributions", params=dict(days=1, show_all=True)
    )

    context = ssl.create_default_context()
    with smtplib.SMTP_SSL(SMTP_SERVER, port, context=context) as server:
        server.login(user=SENDER_EMAIL, password=EMAIL_PASSWORD)
        server.sendmail(
            from_addr=SENDER_EMAIL,
            to_addrs=RECEIVER_EMAIL,
            msg=json.dumps(response.json()),
        )
