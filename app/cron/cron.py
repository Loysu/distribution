import os
import time

import schedule

from app.cron import send_distributions, send_email_stats_to_email

API_HOST = os.getenv("API_HOST")

schedule.every(1).minutes.do(send_distributions)
schedule.every(1).day.do(send_email_stats_to_email)

while True:
    schedule.run_pending()
    time.sleep(1)
