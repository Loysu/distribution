class ObjectAlreadyExist(Exception):
    ...


class ObjectDoesNotExist(Exception):
    ...


class IncorrectData(Exception):
    ...
