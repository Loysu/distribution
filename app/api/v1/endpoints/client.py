from fastapi import APIRouter, Body, Depends, Path, status
from sqlalchemy.orm import Session

from app import schemas, services
from app.api.deps import get_db_pg

router = APIRouter()


@router.get(
    "/{client_id}",
    response_model=schemas.ClientOut,
    status_code=status.HTTP_200_OK,
    summary="Retrieve client by id",
)
async def get_client(
    client_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> schemas.ClientOut:
    return services.get_client(db=db, client_id=client_id)


@router.post(
    "/",
    response_model=schemas.ClientOut,
    status_code=status.HTTP_201_CREATED,
    summary="Create client",
)
async def create_client(
    client: schemas.ClientIn = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.ClientOut:
    return services.create_client(db=db, client=client)


@router.put(
    "/",
    response_model=schemas.ClientOut,
    status_code=status.HTTP_202_ACCEPTED,
    summary="Update client",
)
async def update_client(
    client: schemas.ClientOut = Body(...), db: Session = Depends(get_db_pg)
) -> schemas.ClientOut:
    return services.update_client(db=db, client=client)


@router.delete(
    "/{client_id}",
    status_code=status.HTTP_204_NO_CONTENT,
    summary="Delete client by id",
)
async def delete_client(
    client_id: int = Path(...), db: Session = Depends(get_db_pg)
) -> None:
    return services.delete_client(db=db, client_id=client_id)
