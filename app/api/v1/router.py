from fastapi import APIRouter

from app.api.v1.endpoints import client, distribution, message, statistic, tag

api_router = APIRouter()

api_router.include_router(tag.router, prefix="/tag", tags=["Tags"])
api_router.include_router(client.router, prefix="/client", tags=["Clients"])
api_router.include_router(
    distribution.router, prefix="/distribution", tags=["Distributions"]
)
api_router.include_router(message.router, prefix="/message", tags=["Messages"])
api_router.include_router(statistic.router, prefix="/statistic", tags=["Statistic"])
