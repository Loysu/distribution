import requests

from app import schemas


def send_message_to_client(
    auth_token: str, distribution: schemas.DistributionInForeignAPI
):
    headers = {
        "content-type": "application/json",
        "accept": "application/json",
        "authorization": rf"Bearer {auth_token}",
    }
    data = dict(
        id=distribution.id,
        phone=int(distribution.phone),
        text=distribution.text,
    )

    response = requests.post(
        url="https://probe.fbrq.cloud/v1/send/1", headers=headers, json=data
    )
    if response.status_code == 200:
        return {"result": "ok"}
    return {"result": "error"}


def tags_in_taglist(distribution_tags, client_tags) -> bool:
    flag = True
    if distribution_tags:
        for client_tag in client_tags:
            if client_tag not in distribution_tags:
                flag = False
                break
    return flag
