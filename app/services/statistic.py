from datetime import datetime, timedelta
from typing import Dict

from sqlalchemy.orm import Session

import app.db.base as models
from app import schemas


def get_distributions_stat(
    db: Session, days=1, show_all=False, page=1, unit_per_page=12
) -> Dict:
    """Retrieve statistic.
    Args:
        db:
            database session.
        page:
            page number.
        unit_per_page:
            amount units per page.
        days:
            how many days to issue statistics.
        show_all:
            is show all data without pagination
    Returns:
        Dict[str, str]:
            count messages and distributions data.
    """
    distributions_qs = db.query(models.Distribution).filter(
        models.Distribution.start >= datetime.now() - timedelta(days=days),
        models.Distribution.stop <= datetime.now(),
    )
    messages = 0
    for distribution in distributions_qs:
        messages += len(distribution.messages)

    if show_all:
        distributions = distributions_qs
    else:
        distributions = distributions_qs[
            (page - 1) * unit_per_page : page * unit_per_page
        ]

    return dict(
        amount_messages=messages,
        distributions=[
            schemas.DistributionOut(
                id=distribution.id,
                start=distribution.start,
                stop=distribution.stop,
                message_text=distribution.message_text,
                client_mobile_operator_code=distribution.client_mobile_operator_code,
                tags=[tag.name for tag in distribution.tags],
            )
            for distribution in distributions
        ],
    )


def get_messages_stat(
    db: Session, distribution_id, show_all=False, page=1, unit_per_page=12
) -> Dict:
    """Retrieve messages statistic.
    Args:
        db:
            database session.
        page:
            page number.
        unit_per_page:
            amount units per page.
        distribution_id:
            distribution id.
        show_all:
            is show all data without pagination
    Returns:
        Dict:
            count messages and messages data.
    """
    distribution = db.query(models.Distribution).get(distribution_id)
    if show_all:
        messages = distribution.messages
    else:
        messages = distribution.messages[
            (page - 1) * unit_per_page : page * unit_per_page
        ]

    return dict(
        amount_messages=len(distribution.messages),
        messages=[
            schemas.MessageOut(
                id=message.id,
                delivered=message.delivered,
                state=str(message.state),
                distribution_id=message.distribution_id,
                client_id=message.client_id,
            )
            for message in messages
        ],
    )
