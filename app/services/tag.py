from sqlalchemy.orm import Session

import app.db.base as models
from app import exceptions, schemas


def create_tag(db: Session, tag: schemas.TagIn) -> schemas.TagOut:
    """Create new user.
    Args:
        db:
            database session.
        tag:
            new tag.
    Raises:
        exceptions.ObjectAlreadyExist:
            Tag already exist.
    Returns:
        schemas.TagOut:
            tag data.
    """
    tag_in_db = db.query(models.Tag).filter(models.Tag.name == tag.name).first()
    if tag_in_db:
        raise exceptions.ObjectAlreadyExist("Tag already exist")

    db_tag = models.Tag(**tag.dict())
    models.save(db=db, data=db_tag)
    return schemas.TagOut(**db_tag.__dict__)


def get_tag(db: Session, tag_id: int) -> schemas.TagOut:
    """Get tag.
    Args:
        db:
            database session.
        tag_id:
            tag id.
    Raises:
        exceptions.ObjectDoesNotExist:
            Tag does not exist.
    Returns:
        schemas.TagOut:
            tag data.
    """
    db_tag = db.query(models.Tag).get(tag_id)

    if not db_tag:
        raise exceptions.ObjectDoesNotExist("Tag does not exist")
    return schemas.TagOut(**db_tag.__dict__)


def delete_tag(db: Session, tag_id: int) -> None:
    """Delete tag.
    Args:
        db:
            database session.
        tag_id:
            tag id.
    Raises:
        exceptions.ObjectDoesNotExist:
            Tag does not exist.
    Returns:
        None
    """
    db_tag = db.query(models.Tag).get(tag_id)

    if not db_tag:
        raise exceptions.ObjectDoesNotExist("Tag does not exist")

    db.delete(db_tag)
    db.commit()
