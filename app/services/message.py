from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import app.db.base as models
from app import exceptions, schemas


def create_message(db: Session, message: schemas.MessageIn) -> schemas.MessageOut:
    """Create new message.
    Args:
        db:
            database session.
        message:
            new message.
    Raises:
        exceptions.ObjectAlreadyExist:
            Message already exist.
    Returns:
        schemas.MessageOut:
            created message.
    """

    message_in_db = (
        db.query(models.Message)
        .filter(
            models.Message.delivered == message.delivered,
            models.Message.state == message.state,
            models.Message.distribution_id == message.distribution_id,
            models.Message.client_id == message.client_id,
        )
        .first()
    )

    if message_in_db:
        raise exceptions.ObjectAlreadyExist("Message already exist")

    db_message = models.Message(**message.dict())
    models.save(db=db, data=db_message)
    return schemas.MessageOut(
        id=db_message.id,
        delivered=db_message.delivered,
        state=str(db_message.state),
        distribution_id=db_message.distribution_id,
        client_id=db_message.client_id,
    )


def get_message(db: Session, message_id: int) -> schemas.MessageOut:
    """Retrieve message by id.
    Args:
        db:
            database session.
        message_id:
            message id.
    Raises:
        exceptions.ObjectDoesNotExist:
            Message already exist.
    Returns:
        schemas.MessageOut:
            message data.
    """
    db_message = db.query(models.Message).get(message_id)

    if not db_message:
        raise exceptions.ObjectDoesNotExist("Message does not exist")

    return schemas.MessageOut(
        id=db_message.id,
        delivered=db_message.delivered,
        state=str(db_message.state),
        distribution_id=db_message.distribution_id,
        client_id=db_message.client_id,
    )


def update_message(db: Session, message: schemas.MessageInUpdate) -> schemas.MessageOut:
    """Update message data.
    Args:
        db:
            database session.
        message:
            message data.
    Raises:
        IncorrectData:
            incorrect data.
        exceptions.ObjectDoesNotExist:
            message does not exist.
    Returns:
        schemas.MessageOut:
            updated message data.
    """
    db_message = db.query(models.Message).get(message.id)

    if not db_message:
        raise exceptions.ObjectDoesNotExist("Message does not exist")

    db_message.delivered = message.delivered
    db_message.state = message.state

    try:
        models.save(db=db, data=db_message)
    except IntegrityError:
        raise exceptions.IncorrectData("Incorrect data")

    return schemas.MessageOut(
        id=db_message.id,
        delivered=db_message.delivered,
        state=str(db_message.state),
        distribution_id=db_message.distribution_id,
        client_id=db_message.client_id,
    )


def delete_message(db: Session, message_id: int) -> None:
    """Remove message data.
    Args:
        db:
            database engine.
        message_id:
            message id.
    Raises:
        exceptions.ObjectDoesNotExist:
            message with such id does not exist.
    Returns:
        None
    """
    db_message = db.query(models.Message).get(message_id)

    if not db_message:
        raise exceptions.ObjectDoesNotExist("Message with such id does not exist")

    db.delete(db_message)
    db.commit()
