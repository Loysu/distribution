from datetime import datetime

import pytz
from sqlalchemy.exc import IntegrityError
from sqlalchemy.orm import Session

import app.db.base as models
from app import exceptions, schemas, services, utils


def create_distribution(
    db: Session, distribution: schemas.DistributionIn
) -> schemas.DistributionOut:
    """Create new distribution.
    Args:
        db:
            database session.
        distribution:
            new distribution.
    Raises:
        exceptions.ObjectAlreadyExist:
            Distribution already exist.
    Returns:
        schemas.DistributionOut:
            created distribution.
    """

    distribution_in_db = (
        db.query(models.Distribution)
        .filter(
            models.Distribution.start == distribution.start,
            models.Distribution.stop == distribution.stop,
            models.Distribution.message_text == distribution.message_text,
            models.Distribution.client_mobile_operator_code
            == distribution.client_mobile_operator_code,
        )
        .first()
    )

    if distribution_in_db:
        raise exceptions.ObjectAlreadyExist("Distribution already exist")

    db_distribution = models.Distribution(**distribution.dict(exclude={"tags"}))
    tags = list(db.query(models.Tag).filter(models.Tag.name.in_(distribution.tags)))
    db_distribution.tags = tags
    models.save(db=db, data=db_distribution)
    return schemas.DistributionOut(
        id=db_distribution.id,
        start=db_distribution.start,
        stop=db_distribution.stop,
        message_text=db_distribution.message_text,
        client_mobile_operator_code=db_distribution.client_mobile_operator_code,
        tags=[tag.name for tag in db_distribution.tags],
        messages=[message.id for message in db_distribution.messages],
    )


def get_distribution(db: Session, distribution_id: int) -> schemas.DistributionOut:
    """Retrieve distribution by id.
    Args:
        db:
            database session.
        distribution_id:
            distribution id.
    Raises:
        exceptions.ObjectDoesNotExist:
            Distribution already exist.
    Returns:
        schemas.DistributionOut:
            distribution data.
    """
    db_distribution = db.query(models.Distribution).get(distribution_id)

    if not db_distribution:
        raise exceptions.ObjectDoesNotExist("Distribution does not exist")

    return schemas.DistributionOut(
        id=db_distribution.id,
        start=db_distribution.start,
        stop=db_distribution.stop,
        message_text=db_distribution.message_text,
        client_mobile_operator_code=db_distribution.client_mobile_operator_code,
        tags=[tag.name for tag in db_distribution.tags],
        messages=[message.id for message in db_distribution.messages],
    )


def update_distribution(
    db: Session, distribution: schemas.DistributionOut
) -> schemas.DistributionOut:
    """Update distribution data.
    Args:
        db:
            database session.
        distribution:
            distribution data.
    Raises:
        IncorrectData:
            incorrect data.
        exceptions.ObjectDoesNotExist:
            distribution does not exist.
    Returns:
        schemas.DistributionOut:
            updated distribution data.
    """
    db_distribution = db.query(models.Distribution).get(distribution.id)

    if not db_distribution:
        raise exceptions.ObjectDoesNotExist("Distribution does not exist")

    db_distribution.start = distribution.start
    db_distribution.stop = distribution.stop
    db_distribution.message_text = distribution.message_text
    db_distribution.client_mobile_operator_code = (
        distribution.client_mobile_operator_code
    )
    tags = list(db.query(models.Tag).filter(models.Tag.name.in_(distribution.tags)))
    db_distribution.tags = tags

    try:
        models.save(db=db, data=db_distribution)
    except IntegrityError:
        raise exceptions.IncorrectData("Incorrect data")

    return schemas.DistributionOut(
        id=db_distribution.id,
        start=db_distribution.start,
        stop=db_distribution.stop,
        message_text=db_distribution.message_text,
        client_mobile_operator_code=db_distribution.client_mobile_operator_code,
        tags=[tag.name for tag in db_distribution.tags],
        messages=[message.id for message in db_distribution.messages],
    )


def delete_distribution(db: Session, distribution_id: int) -> None:
    """Remove distribution data.
    Args:
        db:
            database engine.
        distribution_id:
            distribution id.
    Raises:
        exceptions.ObjectDoesNotExist:
            distribution with such id does not exist.
    Returns:
        None
    """
    db_distribution = db.query(models.Distribution).get(distribution_id)

    if not db_distribution:
        raise exceptions.ObjectDoesNotExist("Distribution with such id does not exist")

    db.delete(db_distribution)
    db.commit()


def send_distributions(db: Session, auth_token: str, start: datetime) -> None:
    """Send distributions.
    Args:
        db:
            database engine.
        auth_token:
            auth token for foreign API
        start:
            distributions start time.
    Returns:
        None
    """
    distributions = db.query(models.Distribution).filter(
        models.Distribution.start == start
    )

    for distribution in distributions:
        flag = True
        clients = db.query(models.Client).filter(
            models.Client.mobile_operator_code
            == distribution.client_mobile_operator_code
        )

        for client in clients:
            if not utils.tags_in_taglist(distribution.tags, client.tags):
                continue

            if datetime.now() >= distribution.stop:
                flag = False

            client_message = (
                db.query(models.Message)
                .filter(
                    models.Message.client_id == client.id,
                    models.Message.distribution_id == distribution.id,
                    models.Message.state == "failed",
                )
                .first()
            )

            if not client_message:
                client_timezone = pytz.timezone(client.time_zone)
                utc = pytz.UTC
                distribution_start = utc.localize(distribution.start)
                distribution_stop = utc.localize(distribution.stop)

                if (
                    distribution_start
                    <= datetime.now(client_timezone)
                    <= distribution_stop
                ):
                    msg_state = "failed"

                    if flag:
                        data = dict(
                            id=distribution.id,
                            phone=client.phone_number,
                            text=distribution.message_text,
                        )

                        result = utils.send_message_to_client(
                            auth_token=auth_token,
                            distribution=schemas.DistributionInForeignAPI(**data),
                        )

                        if result["result"] == "ok":
                            msg_state = "success"

                    services.create_message(
                        db=db,
                        message=schemas.MessageIn(
                            delivered=datetime.now(),
                            state=msg_state,
                            distribution_id=distribution.id,
                            client_id=client.id,
                        ),
                    )
