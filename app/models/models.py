import re

from sqlalchemy import Column, DateTime, ForeignKey, Integer, String, Table
from sqlalchemy.orm import RelationshipProperty, relationship, validates
from sqlalchemy_utils import ChoiceType

from app.db.base_class import Base

distribution_and_tag_table = Table(
    "distribution_and_tag",
    Base.metadata,
    Column("distribution_id", ForeignKey("distribution.id"), primary_key=True),
    Column("tag_id", ForeignKey("tag.id"), primary_key=True),
)
client_and_tag_table = Table(
    "client_and_tag",
    Base.metadata,
    Column("client_id", ForeignKey("client.id"), primary_key=True),
    Column("tag_id", ForeignKey("tag.id"), primary_key=True),
)


class Distribution(Base):
    __tablename__ = "distribution"

    id = Column(Integer, primary_key=True)
    start = Column(DateTime, nullable=False)
    stop = Column(DateTime, nullable=False)
    message_text = Column(String, nullable=False)
    client_mobile_operator_code = Column(Integer, nullable=False)

    tags: RelationshipProperty = relationship(
        "Tag", secondary=distribution_and_tag_table, back_populates="distributions"
    )
    messages: RelationshipProperty = relationship(
        "Message", back_populates="distribution"
    )


class Tag(Base):
    __tablename__ = "tag"

    id = Column(Integer, primary_key=True)
    name = Column(String, unique=True, nullable=False)

    distributions: RelationshipProperty = relationship(
        "Distribution", secondary=distribution_and_tag_table, back_populates="tags"
    )
    clients: RelationshipProperty = relationship(
        "Client", secondary=client_and_tag_table, back_populates="tags"
    )


class Client(Base):
    __tablename__ = "client"

    id = Column(Integer, primary_key=True)
    phone_number = Column(String, nullable=False)
    mobile_operator_code = Column(Integer, nullable=False)
    time_zone = Column(String, default="UTC")

    tags: RelationshipProperty = relationship(
        "Tag", secondary=client_and_tag_table, back_populates="clients"
    )
    messages: RelationshipProperty = relationship("Message", back_populates="client")

    @validates("phone_number")
    def validate_phone_number(self, key, address):
        phone_number = re.fullmatch(r"(\+7|8)\d{10}", address)
        if not phone_number:
            raise ValueError("Invalid phone number format")
        return address


class Message(Base):
    __tablename__ = "message"

    id = Column(Integer, primary_key=True)
    delivered = Column(DateTime, nullable=False)

    STATE_CHOICE = (
        ("success", "Success"),
        ("failed", "Failed"),
    )
    state = Column(ChoiceType(STATE_CHOICE, impl=String()))

    distribution_id = Column(Integer, ForeignKey("distribution.id", ondelete="CASCADE"))
    distribution: RelationshipProperty = relationship(
        "Distribution", back_populates="messages"
    )

    client_id = Column(Integer, ForeignKey("client.id", ondelete="CASCADE"))
    client: RelationshipProperty = relationship("Client", back_populates="messages")
