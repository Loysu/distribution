from datetime import datetime
from typing import List, Optional

from app.schemas import BaseSchema


class TagIn(BaseSchema):
    name: str


class TagOut(BaseSchema):
    id: int
    name: str


class ClientIn(BaseSchema):
    phone_number: str
    mobile_operator_code: int
    time_zone: Optional[str]
    tags: List[str]


class ClientOut(BaseSchema):
    id: int
    phone_number: str
    mobile_operator_code: int
    time_zone: Optional[str]
    tags: List[str]


class DistributionIn(BaseSchema):
    start: datetime
    stop: datetime
    message_text: str
    client_mobile_operator_code: int
    tags: Optional[List[str]]


class DistributionInForeignAPI(BaseSchema):
    id: int
    phone: str
    text: str


class DistributionOut(BaseSchema):
    id: int
    start: datetime
    stop: datetime
    message_text: str
    client_mobile_operator_code: int
    tags: Optional[List[str]]
    messages: Optional[List[int]] = []


class MessageIn(BaseSchema):
    delivered: datetime
    state: str
    distribution_id: int
    client_id: int


class MessageInUpdate(BaseSchema):
    id: int
    delivered: datetime
    state: str


class MessageOut(BaseSchema):
    id: int
    delivered: datetime
    state: str
    distribution_id: int
    client_id: int
